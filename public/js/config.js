require.config({
    baseUrl: "/js",
    deps: ['app', 'utils/notifications'],
  //  urlArgs: "bust=" + (new Date()).getTime(), //Avoiding from files to being cached
    paths: {
        angular: 'vendors/angular/angular.min',
        'angular-resource': 'vendors/angular-resource/angular-resource',
        'angular-bootstrap':'vendors/angular-bootstrap/ui-bootstrap-tpls',
        restangular: 'vendors/restangular/restangular',
        uiRouter: 'vendors/angular-ui-router/angular-ui-router',
        jquery: 'vendors/jquery/jquery.min',
        underscore: 'vendors/lodash/lodash.compat',
        text: 'vendors/requirejs-text/text',
        plupload: 'vendors/plupload/plupload.full.min',
        noty: 'vendors/noty/jquery.noty.packaged.min',
        notify: 'app/utils/notifications',
        icheck: 'vendors/icheck/icheck.min'
    },
    shim: {
        'angular': {
            'exports': 'angular'
        },
        'angular-bootstrap':['angular'],
        'restangular': ['angular', 'underscore'],
        'uiRouter': ['angular'],
        'angular-resource': ['angular'],
        noty: {depends: ['jquery'], exports: 'noty'}
    },
    priority: [
        "angular"
    ]
});

window.name = "NG_DEFER_BOOTSTRAP!";