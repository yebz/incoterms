define([
        'angular',
        'admin/routes/AdminRoutes',
        'admin/controllers/AddUserController',
        'admin/controllers/ListUserController',
        'admin/controllers/UpdateUserController'
    ],
    function (angular, AdminRoutes, AddUserController, ListUserController, UpdateUserController) {

        "use strict";

        var techModule = "ers.Admin";

        var module = angular.module(techModule, ['ui.router'])
            .controller('addUserController', AddUserController)
            .controller('listUserController', ListUserController)
            .controller('updateUserController', UpdateUserController)
            .config(AdminRoutes);

        return techModule;
    });