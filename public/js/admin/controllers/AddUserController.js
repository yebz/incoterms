define([ 'underscore', 'notify'],
    function (_, notify) {
        "use strict";

        var AddUserController = function ($state, $scope, Restangular) {
            var User = Restangular.all('admin/user');

            $scope.saveUser = function () {
                User.post($scope.user).then(function () {
                    notify.info('User successfully saved!');
                    $scope.user = {};
                }, function () {
                    notify.error('Error adding user!');

                });
            }

            $scope.clear = function () {
                $scope.user = {};
            }

        }
        return ["$state", "$scope", "Restangular", AddUserController];
    });