define([ 'underscore', 'notify'],
    function (_, notify) {
        "use strict";

        var ListUserController = function ($state, $scope, Restangular) {
            var User = Restangular.all('admin/user');

            User.getList().then(function (users) {
                $scope.userList = users;
            }, function (error) {
                notify.error('Error retrieving users list...');
            });


            $scope.remove = function (user) {
                user.remove().then(function () {
                    var userlist = $scope.userList;
                    $scope.userList = _.without(userlist, _.findWhere(userlist, {uuid: user.uuid}));
                }, function () {
                    notify.error("Error removing user...");

                });

            }

            $scope.reset = function(uuid){
                Restangular.one('admin/user', uuid).post('resetPassword').then(function () {
                    notify.info('The new password has been sent by email...')
                }, function () {
                    notify.error('There was a problem resetting the password...')
                })
            }

            $scope.resetPassword = function () {
                var selectedUser = $scope.resetUser;
                Restangular.one('admin/user', selectedUser).post('resetPassword').then(function () {
                    notify.info('The new password has been sent by email...')
                }, function () {
                    notify.error('There was a problem resetting the password...')
                })
            }

        }
        return ["$state", "$scope", "Restangular", ListUserController];
    });