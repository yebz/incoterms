(function (define) {
    "use strict";
    define([],
        function () {
            var RouterManager = function ($stateProvider) {
                $stateProvider
                    .state('app.admin', {
                        url: '/admin',
                        views: {
                            'breadcrumbs': {
                                templateUrl: 'js/admin/views/breadcrumbs.tpl.html'
                            },
                            'content@': {
                                templateUrl: 'js/admin/views/index.tpl.html'
                            }

                        }
                    }).state('app.admin.adduser', {
                        url: '/adduser',
                        views: {
                            'content@': {
                                templateUrl: 'js/admin/views/adduser.tpl.html'
                            }
                        }
                    }).state('app.admin.listuser', {
                        url: '/listuser',
                        views: {
                            'content@': {
                                templateUrl: 'js/admin/views/userslist.tpl.html'
                            }
                        }
                    }).state('app.admin.resetpass', {
                        url: '/resetpass',
                        views: {
                            'content@': {
                                templateUrl: 'js/admin/views/resetpassword.tpl.html'
                            }
                        }
                    }).state('app.admin.updateuser', {
                        url: '/updateuser/{uuid}',
                        views: {
                            'content@': {
                                templateUrl: 'js/admin/views/updateuser.tpl.html'
                            }
                        }
                    })

            }
            return ['$stateProvider', RouterManager];
        });
}(define));