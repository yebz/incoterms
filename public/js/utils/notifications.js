define('notify', ['noty'],
    function (noty) {
        "use strict";

        var notify = {
            info: function (msg) {
                noty({text: msg, timeout: 1000, maxVisible: 1, template: '<div class="notification info"><span class="noty_text"></span><div class="noty_close"></div></div>' })
            },
            warning: function (msg) {
                noty({text: msg, timeout: 1000, maxVisible: 1, template: '<div class="notification warning"><span class="noty_text"></span><div class="noty_close"></div></div>' })
            },
            error: function (msg) {
                noty({text: msg, timeout: 1000, maxVisible: 1, template: '<div class="notification danger"><span class="noty_text"></span><div class="noty_close"></div></div>' })
            },
            system: function (msg) {
                noty({text: msg, timeout: 1000, maxVisible: 1, template: '<div class="nofitication system"><span class="noty_text"></span><div class="noty_close"></div></div>' })
            }


        }

        return notify;


    });