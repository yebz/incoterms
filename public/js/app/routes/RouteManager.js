(function(define) {
    "use strict";
    define([
        ],
        function() {

            var RouterManager = function($stateProvider) {
                $stateProvider.state('app', {
                    url: '',
                    views: {
                        'header': {
                            templateUrl: 'js/app/views/header.tpl.html'
                        },
                        'sidebar': {
                            templateUrl: 'js/app/views/sidebar.tpl.html'
                        },
                        'content@': {
                            templateUrl: 'js/app/views/dashboard.tpl.html'
                        }
                    }
                });
            }
            return ['$stateProvider', RouterManager];
        });
}(define));