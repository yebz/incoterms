define([ 'underscore', 'notify'],
    function (_, notify) {
        "use strict";

        var NavBarController = function ($state, $scope) {


            $scope.go = function (path) {
                $scope.isCollapsed = true;
                $state.go(path);
//                $location.path('#/' + path);
            }

        }
        return ["$state", "$scope", NavBarController];
    });
/**
 * Created by darioherrera on 01/07/14.
 */
