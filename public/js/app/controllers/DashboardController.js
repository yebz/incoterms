define([ 'underscore', 'notify'],
    function (_, notify) {
        "use strict";

        var DashBoardController = function ($state, $scope) {

            $scope.test = function () {
                notify.info('saving data');

            }

        }
        return ["$state", "$scope", DashBoardController];
    });