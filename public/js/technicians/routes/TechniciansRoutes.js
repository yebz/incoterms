(function (define) {
    "use strict";
    define([],
        function () {
            var RouterManager = function ($stateProvider) {
                $stateProvider
                    .state('app.tech', {
                        url: '/technicians',
                        views: {
                            'content@': {
                                templateUrl: 'js/technicians/views/index.tpl.html'
                            }

                        }
                    }).state('app.tech.qc', {
                        url: '/qualitycontrol',
                        views: {
                            'content@': {
                                templateUrl: 'js/technicians/views/qualitycontrol.tpl.html'
                            }
                        }
                    }).state('app.tech.qr', {
                        url: '/qualityreports',
                        views: {
                            'content@': {
                                templateUrl: 'js/technicians/views/qualityreports.tpl.html'
                            }
                        }
                    });

            }
            return ['$stateProvider', RouterManager];
        });
}(define));