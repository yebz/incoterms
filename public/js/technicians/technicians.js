define([
        'angular',
        'technicians/routes/TechniciansRoutes',
        'technicians/controllers/QualityControlController'
    ],
    function (angular, TechniciansRoutes, QualityControlController) {

        "use strict";

        var techModule = "ers.Technicians";

        var module = angular.module(techModule, ['ui.router'])
            .controller('qualityControlController', QualityControlController)
            .config(TechniciansRoutes);

        return techModule;
    });