define([
    'angular',
    'app/routes/RouteManager',
    'app/controllers/NavBarController',
    'technicians/technicians',
    'admin/admin',
    'restangular',
    'uiRouter',
    'jquery',
    'angular-bootstrap'
], function (angular, RouteManager, NavBarController, TechniciansModule, AdminModule, $) {
    'use strict';
    var appName = 'ers';
    var depends = [ 'ui.router', 'restangular', 'ui.bootstrap.collapse', TechniciansModule, AdminModule];

    // Declare app level module which depends on filters, and services
    var app = angular.module(appName, depends)
        .config(function (RestangularProvider) {
            RestangularProvider.setRestangularFields({
                id: "uuid"
            });
        })
        .config(RouteManager)
        .controller('NavBarCtrl', NavBarController);


    angular.bootstrap(document.getElementsByTagName("body")[0], [appName]);
    return app;
});