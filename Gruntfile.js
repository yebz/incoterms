module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            development: {
                options: {
                    paths: ["assets/css"],
                    compress: true
                },
                files: {
                    "public/assets/css/result.css": "styles/less/flat-ui.less"
                }
            }
        }

//        cssmin: {
//            minify: {
//                expand: true,
//                cwd: 'public/assets/css/',
//                src: ['*.css', '!*.min.css'],
//                dest: 'public/dist/css/',
//                ext: '.min.css'
//            },
//            combine: {
//                files: {
//                    'public/dist/css/guabara.css': ['public/dist/css/bootflat.min.css', 'public/dist/css/main.min.css']
//                }
//            }
//        }
    });

//    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('test', ['less']);


};