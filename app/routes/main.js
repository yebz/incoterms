module.exports = {

    /**
     * main page
     * @param  {[type]} req [description]
     * @param  {[type]} res [description]
     * @return {[type]}     [description]
     */
    index: function (req, res) {
        var params = {};
        res.render('index', params)
    },


    admin: function (req, res) {
        res.render('admin/index', {
            layout: 'admin'
        });
    },

    login: function (req, res) {
        res.render('login');
    },

    logout: function (req, res) {
        req.logout()
        res.redirect('/login')
    },

    adminlogin: function (req, res) {
        var redirectTo = req.session.returnTo ? req.session.returnTo : '/'
        delete req.session.returnTo
        res.redirect(redirectTo)
    },


    NotFound: function (err, req, res, next) {
        res.status(err.status || 404);
        res.render('error', {
            error: err.status
        });
    },

    table: function (req, res) {
        res.render('table');
    },




}