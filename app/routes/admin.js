var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Passwordgen = require('passwordgen'),
    email = require("../utils/email");
gen = new Passwordgen();

module.exports = {

    saveUser: function (req, res) {
        var user = new User(req.body);
        user.save(function () {
            console.log(user);
            if (req.body.notify) {
                email.notifyNewUSer(user.email, user, function () {
                    res.json(200, { username: user.username, name: user.name, email: user.email});
                });
            }

            res.json(200, { username: user.username, name: user.name, email: user.email});
        });
    },

    userlist: function (req, res) {
        var q = User.find({}, 'uuid created active role techid username email name');
        q.exec(function (err, doc) {
            res.send(200, doc);
        });
    },
    removeuser: function (req, res) {
        var uuid = req.param('uuid');

        User.findOne({uuid: uuid}, function (err, user) {
            user.remove();
            res.send(200);
        });

    },

    resetPassword: function (req, res) {
        var newPass = gen.chars();
        var uuid = req.param('uuid');
        User.findOne({uuid: uuid}, function (err, user) {
            user.password = newPass;
            user.save(function () {
                console.log(newPass);
                email.sendPassword(user.email, {password: newPass}, function () {
                    res.send(200, "ok");
                });

            });


        });
    },

    findUserById: function(req,res){
        var uuid = req.param('uuid');
        var q = User.find({}, 'uuid created active role techid username email name');
        q.exec(function (err, doc) {
            res.send(200, doc);
        });
    }


}