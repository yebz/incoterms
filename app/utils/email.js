var env = process.env.NODE_ENV || 'development',
    config = require('../../config/config')[env],
    postmark = require("postmark")(config.postmark.POSTMARK_API_KEY),
    handlebars = require('handlebars'),
    fs = require('fs');


var send = function (emails, title, content, cb) {
    postmark.send({
        "From": "contacto@guabara.com",
        "To": emails,
        "Subject": title,
        "HtmlBody": content
    }, function (error, success) {
        if (error) {
            console.error("Unable to send via postmark: " + error.message);
            return;
        }
        console.info("Sent to postmark for delivery");
        cb();
    });
}

module.exports = {

    sendPassword: function (email, password, cb) {
        var orderFileDir = __dirname + '/../../templates/changePassword.hbs';
        fs.readFile(orderFileDir, 'utf-8', function (err, data) {
            if (err) throw err;
            var template = handlebars.compile(data);
            send(email, 'Your password has been reset', template(password), cb);

        });

    },

    notifyNewUSer: function (email, user, cb) {
        var orderFileDir = __dirname + '/../../templates/addUser.hbs';
        fs.readFile(orderFileDir, 'utf-8', function (err, data) {
            if (err) throw err;
            var template = handlebars.compile(data);
            send(email, 'Welcome to premier installer', template(user), cb);

        });

    }


};