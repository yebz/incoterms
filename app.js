var express = require('express'),
    env = process.env.NODE_ENV || 'development',
    config = require('./config/config')[env],
    mongoose = require('mongoose'),
    fs = require('fs'),
    passport = require('passport'),
    app = express();

/**
 * Connecting database
 */
var connect = function () {
    var options = {
        server: {
            socketOptions: {
                keepAlive: 1
            }
        }
    }
    mongoose.connect(config.database, options);
    console.log("Database connected");
}

connect();
// Error handler
mongoose.connection.on('error', function (err) {
    console.error(err)
})

// Reconnect when closed
mongoose.connection.on('disconnected', function () {
    connect();
})

// Bootstrap models
var models_path = __dirname + '/app/models'
fs.readdirSync(models_path).forEach(function (file) {
    if (~file.indexOf('.js')) require(models_path + '/' + file)
});

require('./config/passport')(passport, config);
require('./config/express')(app, config, passport, mongoose.connection);
exports = module.exports = app;