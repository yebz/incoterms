var express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    mongoStore = require('connect-mongostore')(session),
    bodyParser = require('body-parser'),
    compress = require('compression'),
    multer = require('multer');


//Internal workaround
var rootPath = path.normalize(__dirname + '/..');


module.exports = function (app, config, passport, mongooseConn) {

    // view engine setup
    app.use(multer({
        dest: './uploads/'
    }))


    app.set('view engine', 'ejs');

    // app.use(favicon());
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());
    app.use(cookieParser());
    app.use(compress());
    app.use(express.static(path.join(rootPath, 'public')));


    // express/mongo session storage
    app.use(session({
        secret: config.cookieSecret,
        cookie: { maxAge: 620000 },
        store: new mongoStore({
            mongoose_connection: mongooseConn,
            host: 'kahana.mongohq.com',
            port: '10056',
            db: 'ers_sandbox',
            username: 'admin',
            password: 'bratTAiN5',
            collection: 'sessions'
        })
    }))


    //Minify middleware


    //Passport Configuration
    app.use(passport.initialize())
    app.use(passport.session())


    // Handle 404
    app.use(function (error, req, res, next) {
        res.status(400);
        console.log(error);
        res.render('error', {
            error: err.status
        });
    });

    // Handle 500
    app.use(function (error, req, res, next) {
        console.log("error");
        res.status(500);
        console.log(error);
        res.render('error', {
            error: error.status
        });
    });
    require('./routes')(app, passport);


}