var main = require('../app/routes/main');
var admin = require('../app/routes/admin');
var auth = require('./middlewares/authorization');


module.exports = function (app, passport) {

    //Common functionalitites
    app.get('/', main.index);
//    app.get('/', auth.requiresLogin, main.index);


    app.route('/login').get(main.login);

    //Login
    app.post('/users/session', passport.authenticate('local', {
        failureRedirect: '/login'
    }), main.adminlogin);

    //Logout
    app.get('/logout', main.logout);
//
//    //Admin
    app.route('/admin/user').post(admin.saveUser);
    app.route('/admin/user').get(admin.userlist);
    app.route('/admin/user/:uuid').delete(admin.removeuser);
    app.route('/admin/user/:uuid/resetPassword').post(admin.resetPassword);
//

    //Forms

//    //Move this to API
//    app.route('/productos/uploadImage').post(product.uploadImage);
//    //API
//    app.route('/api/products').get(api.productList);
//    app.route('/api/product').post(api.saveProduct);
//    app.route('/api/product/:uuid').delete(api.deleteProduct);
//    app.route('/api/product/:uuid').get(api.producyById);

//    app.route('/api/rate').get(api.getRate);
//}

}